#!/usr/bin/ruby

(0..200).each do |i|
	nome = ''
	if i % 5 == 0
		nome = 'Santa'
	end
	if i % 6 == 0
		nome = nome + 'rem'
	end
	puts "#{i}: #{nome}"
end